$.fn.progressBar = function() {

    return this.each(function() {

        var $init = $(this);

        $init.addClass("progress-bar");

        var progressBarDefault = {
            duration: 10000,
            endNumber: 100
        };

        var settings = $.extend(progressBarDefault, $init.data());

        $init.each(function() {
            var $el = $(this);

            var $number = $('<span>', $el).addClass('progress-bar__number title-h2');
            $el.append($number);

            var $slice_bar = $('<div>', $el).addClass('slice-bar');
            $el.append($slice_bar);

            var $slice_fill = $('<div>', $el).addClass('slice-fill');
            $el.append($slice_fill);

            var $bar = $('<div>', $el.find('.slice-bar')).addClass('bar');
            $el.find('.slice-bar').append($bar);

            var $fill = $('<div>', $el.find('.slice-fill')).addClass('fill');
            $el.find('.slice-fill').append($fill);

            var $progress_bar_after = $('.progress-bar::after');
            var _size_after = 185;

            var _size = $el.width();
            var _number = settings.endNumber;
            var _rate = settings.duration / 100;
            var _val = $number.text();
            var _rotate = 0;

            $slice_bar.css("clip", "rect(0," + _size + "px," + _size + "px," + _size / 2 + "px)");

            $bar.css("clip", "rect(0," + _size / 2 + "px," + _size + "px,0)");

            $progress_bar_after.css("clip", "rect(0," + _size_after / 2 + "px," + _size_after + "px,0)");


            var runTimes = setInterval(function() {

                if (_val === _number) {
                    clearInterval(runTimes);
                    return false;
                }

                _val++;
                _rotate = _val / 100 * 360;

                if (_rotate > 180) {
                    $fill.css({
                        clip: "rect(0," + _size / 2 + "px," + _size + "px,0)",
                        transform: "rotate(" + _rotate + "deg)"
                    });
                    $progress_bar_after.css({
                        clip: "rect(0," + _size_after / 2 + "px," + _size_after + "px,0)",
                        transform: "rotate(" + _rotate + "deg)"
                    });
                } else {
                    $bar.css("transform", "rotate(" + _rotate + "deg)");
                    $progress_bar_after.css("transform", "rotate(" + _rotate + "deg)");

                }

                $number.text(_val + "%");
            }, _rate);
        });
    });
};


$('[data-init="progressBar"]').progressBar();
