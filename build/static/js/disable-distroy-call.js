(function($){
			$(window).on("load",function(){

                var val=$(".about-me");

                $(val).mCustomScrollbar("scrollTo",val);

				$(".wrapper.content").mCustomScrollbar({
					//theme: "dark",
					setHeight: "100vh",
                    mouseWheel: true
				});

                $('.mCSB_container').css({
                    'margin': '0'
                });

                $('.mCSB_scrollTools').css({
                    'position': 'absolute',
					'top': '0',
					'left': '50%',
                    'background': 'url("static/img/scroll-line.png")',
                    'background-position': '0 0',
                    'background-repeat': 'repeat-y'
				});

				$('.mCSB_dragger_bar').css({
					'display': 'block',
					'margin-left': '-15px',
					'background': 'url("data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJIAAAECCAQAAAC/Y0byAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfiAwkMMRkG3HNlAAAIKklEQVR42u2c4XniOBCGv6UCl+AOUAdxCXRwTgeU4KuA5ypwrgJyFZhUgKnAdGA68P1gWciGoE+y5BHeef3jdu+xZc/LaDwSyf6AHAYZCgBLZACKO2fsAJxwALDDCa3Ug/4QUFNgCQPjcW2LFgfsptY1naQcK7ygQBZgrBN2+MA7jpM9fXQMNugwRDg6bLwyMilyVJH0fFZVIZcO1Y8Vmuh6bo8GK+mQXciwniB/7ufUOkjNiy6oQi8i6HL0qNIWJS3oVlSSlEJT7PupV0or+YyZuEizR5NOg1CJy3h0VNJ6AIO9uAbbsZfNp7RzKIF8yhKtQ98dzfSNQZHIy97l6O9uxxD47QKUqINrP35Z0+cRVmSveHO/yEdSHagDOaLFAS1O2D04q0AGgyVMIGVveA0yzgMybEenfYcapUfIOUrUAVrWbdzqlI184YfZ/xm/P7WPp2mMoh514G7FoB7x8oikyV9RhzLSJ5eNWDFG0OSraIplpq+owJr8FE25DvcTFVTT1qMGVZMJuuCzm7UNdfPa+daN0MZ87rFUCtIWr51zaC0i6Pq8rvlUjr1l4TzLc1FFAJA7V9BizO0yx09lI+3nFxvH7M/8b+XyifSJ7SmXjjPAE5cttT6d3eRfGKd5UPncwqUadQkqOmvqYlamzGH4iMvF0bi0wZ1rHHzhS1mRqyanF4+JZ19EEz8rDD9s88Tl+h58CW/YIdmX57MoctNUMsPxyUkNlwzsR08VELY7Sqe7ZmFfRpVtIHYh4t2hisK956yLFC6P+gSWsT7kZApUjwZh86iUjtYbbuPnYS5xeUS/JpOkGZtLzHvtWafaBW7Kdd9dzr0kK4cHShNuvpT3L2YSsXN6nFRhZszdopLPvGTfws2Z/OuFmz8mjwAulzZ+l5XSsQWDyaUvKcFsjswnjwAuKcz51MXPS/4ihv1bOq6gMNH8ZsXuddTXLgnCrC4+zR1msoX/KUlpanbCnadbQQz5j3RMwWEiKq5/tLeR8yraF+xF5qal9OoZZgDTG/6E+RrSSMcTBaYWF+eaZBdwlPuHC6LSEr80b86SltYTd9LRRMMe2ZLNpA/pWKJhj8yc/2Ofl7l0LNFg9j7AFK9eOpKo2PtusyAWG610HFGxR5ctiG57vhWJi65YEMMcpeOIChHdAi8hhnli7NG9MJnUSscRFSI6RtJJOo6oUNH92Q0AQDQB9kxqpWOIjjVCZrr98agkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSQQqiUAlEagkApVEoJIIVBKBSiJQSRSD5eilHzA6vc3BAjvLEJl0DNGxRbhjppttkOeGiI6RZKTjiAoR3QIf1nNy6TiiYo/ug8kk+zDPDBGdvXADL9JxRMUe3W6Bk/UkIx1HVOzRnQB7pzTMeMLlRPRYAGitQxXSsUTDHlnLSppvVbJH1p4lHawnFtKxRMMe2YHNpHymxdsQ1ba9/MFevDbS8URhw5TtC4311E46nih01rgb4LJ2+8863BwnHDPZ/rs93Z52tXRMwamJqM3tBfbE62e2ZZLZN9suReaywH0nBl1JxxWUFfGhv3/+KzPh5lW87XNn+FqHmYtK6ciCUfolBdMzzCeXmJS40xsy6+G55BKTR9/sfdhbyrnkEpNHzRi/lXSEo6nGzRnGcP/kW3A50R/9NmM+fxHwL3GT7Ml775pqih+YYLrQAQPW0pF6s6bis6wuuPn6rFOOm2rWusvm0l46Xi/2IfIIYHPpGbfhNmRklX2ojHrHPXxJJgnX4AzouN0Odrj+iTbiDFlGHD76ZmaaeEWNy6DckHRyisIXkMHtQ2eL3IB94poy8p3m8TJysZ+yJhdFHrOioAcf0CVam4zDR+041S6wHVOqJZwv1wNG7G7wqTogtb6JbWQuJcMbdpHiWfgiwr94zvMgH3Mzl8p0/kRG3S4IueMMGMb/1Ixb2g7ohTdS1o7ZH6hM1I43HdAI5VNOrxWuR7AtxK3zrXuBvfDKOYcGbMPd3qUlux7dhO+70qkjulbQLORD+GmaRpSfoCgrBV9NZ1HBH+fnM/kKiraY8tc0oEcduCc3qD1qUGRFYzWdc2oTQJXBxjt/ois6a9qOeryzqhqlR5OQo0Q9Us+AAVtXRT88RNWBivERLQ5ocXr4S0AFMhgsqZ9wZHjDq+slPpKAMsK3uEccf/s/eYSW9BVvwcf8lmJE0ZQ6+ul/syHzWAJIHo3U/qnLtpzsUckIOmNGNgVTHPsUdk3TzifRHLrFJFqfmhRy6Bb/VVScY8o9CCd8dnRiHBK7WQ5k4qJ6VAl/WXojai009Tqsn0HQldXExbx51l8QylFNkFMdqgS+yBrJ+P2f7/WE2J+y4rcL4EOOFV5QBKkZJ+zwgfcv+waRmE7SBYMCSxivDGjR4oAd8Uv6QZle0hWDDAUu/8pDceeMHQDgA8AOp6nVXPkfJP2na8MDWR0AAAAASUVORK5CYII=")',
					'height': '58px',
					'width': '33px',
					'background-size': 'cover',
					'cursor': 'pointer',
					'z-index': '100'
				});

				$('.mCSB_draggerRail').css({
					'display': 'block',
					'background-color': 'rgba(0, 0, 0, 0)',
					'width': '4px',
					'height': '25px'
				});

				$('.mCSB_dragger').css({
					'width': '33px',
					'min-height': '58px',
					'height': '1200px'
				});

				$('#mCSB_1_dragger_vertical .mCSB_dragger').css({
					'min-height': '58px',
					'height': '200px'
				});


				$(".disable-destroy a").click(function(e){
					e.preventDefault();
					var $this=$(this),
						rel=$this.attr("rel"),
						el=$(".content"),
						output=$("#info > p code");
					switch(rel){
						case "toggle-disable":
						case "toggle-disable-no-reset":
							if(el.hasClass("mCS_disabled")){
								el.mCustomScrollbar("update");
								output.text("$(\".content\").mCustomScrollbar(\"update\");");
							}else{
								var reset=rel==="toggle-disable-no-reset" ? false : true;
								el.mCustomScrollbar("disable",reset);
								if(reset){
									output.text("$(\".content\").mCustomScrollbar(\"disable\",true);");
								}else{
									output.text("$(\".content\").mCustomScrollbar(\"disable\");");
								}
							}
							break;
						case "toggle-destroy":
							if(el.hasClass("mCS_destroyed")){
								el.mCustomScrollbar();
								output.text("$(\".content\").mCustomScrollbar();");
							}else{
								el.mCustomScrollbar("destroy");
								output.text("$(\".content\").mCustomScrollbar(\"destroy\");");
							}
							break;
					}
				});
				
			});
		})(jQuery);

