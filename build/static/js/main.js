var g_val = [],
    g_number = [],
    g_rotate = [],
    g_size = [],
    g_size_after = [],
    g_rate = [],
    g_progress_bar_after = [],
    g_bar = [],
    g_numberO = [],
    g_fill = [],
    allProgressBars = [];

$.fn.progressBar = function() {

    return this.each(function() {

        var $init = $(this);

        $init.addClass("progress-bar");

        allProgressBars.push($init);

        var progressBarDefault = {
            duration: 10000,
            endNumber: 100
        };

        var settings = $.extend(progressBarDefault, $init.data());

        $init.each(function() {
            var $el = $(this);

            var $number = $('<span>', $el).addClass('progress-bar__number title-h2');
            $el.append($number);

            var $slice_bar = $('<div>', $el).addClass('slice-bar');
            $el.append($slice_bar);

            var $slice_fill = $('<div>', $el).addClass('slice-fill');
            $el.append($slice_fill);

            var $bar = $('<div>', $el.find('.slice-bar')).addClass('bar');
            $el.find('.slice-bar').append($bar);

            var $fill = $('<div>', $el.find('.slice-fill')).addClass('fill');
            $el.find('.slice-fill').append($fill);

            var $progress_bar_after = $('.progress-bar::after');
            var _size_after = 185;

            var _size = $el.width();
            var _number = settings.endNumber;
            var _rate = settings.duration / 100;
            var _val = 0;//$number.text();
            var _rotate = 0;

            g_val.push(_val);
            g_number.push(_number);
            g_rotate.push(_rotate);
            g_size.push(_size);
            g_size_after.push(_size_after);
            g_rate.push(_rate);
            g_progress_bar_after.push($progress_bar_after);
            g_bar.push($bar);
            g_numberO.push($number);
            g_fill.push($fill);

            $slice_bar.css("clip", "rect(0," + _size + "px," + _size + "px," + _size / 2 + "px)");

            $bar.css("clip", "rect(0," + _size / 2 + "px," + _size + "px,0)");

            $progress_bar_after.css("clip", "rect(0," + _size_after / 2 + "px," + _size_after + "px,0)");

        });
    });
};

$(document).ready(function () {

    $('.about-me_progress').viewportChecker({
        callbackFunction: function () {
            $('.progress-text1').text('Mustache butcher direct trade, locavore banjo flexitarian taxidermy swag poutine literally. Selvage bushwick pour-over hella, austin kogi tumblr 90\'s chartreuse tacos.');
            $('.progress-text2').text('Viral actually PBR&B, portland wayfarers gluten-free pour-over. Pop-up kombucha chicharrones, YOLO mixtape celiac squid. Heirloom plaid everyday carry mustache.');
            $('.progress-text3').text('Pitchfork distillery next level, polaroid roof party austin fanny pack knausgaard everyday carry ethical freegan plaid yuccie brunch. Blue bottle wayfarers umami messenger.');
        }
    });

    //$('[data-init="progressBar"]').progressBar();


    function fillBars() {
        var i = 0;
        return $('[data-init="progressBar"]').each(function(i) {
            var $init = $(this);
            $init.addClass("progress-bar");

            var progressBarDefault = {
                duration: 10000,
                endNumber: 100
            };

            var settings = $.extend(progressBarDefault, $init.data());

            $init.each(function() {
               var runTimes = setInterval(function () {
                   if (g_val[i] < g_number[i]) {
                       g_val[i]++;
                       g_rotate[i] = g_val[i] / 100 * 360;

                       if (g_rotate[i] > 180) {
                           //alert(g_rotate);
                           $(g_fill[i]).css({
                               clip: "rect(0," + g_size[i] / 2 + "px," + g_size[i] + "px,0)",
                               transform: "rotate(" + g_rotate[i] + "deg)"
                           });
                           $(g_progress_bar_after[i]).css({
                               clip: "rect(0," + g_size_after[i] / 2 + "px," + g_size_after[i] + "px,0)",
                               transform: "rotate(" + g_rotate[i] + "deg)"
                           });
                       } else {
                           //alert(g_rotate);
                           $(g_bar[i]).css("transform", "rotate(" + g_rotate[i] + "deg)");
                           $(g_progress_bar_after[i]).css("transform", "rotate(" + g_rotate[i] + "deg)");
                       }
                       $(g_numberO[i]).text(g_val[i] + "%");
                   }
               }, g_rate[i]);

            });
        });
        i++;
    }

    var r;
    var wow = new WOW({
        boxClass:     'wow',
        animateClass: 'animated',
        offset:       0,
        mobile:       true,
        live:         false,
        //callback: fillBars()
    });
    wow.init();



    //title-banner slider
    $('.title-banner_slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        speed: 3000,
        arrows: false
    });

    $('.slick-dots').find('li').find('button').html('');

    //size measures for slider
    $('.slider-window').css({
        'height': $('.slider-window').width() * 0.4968
    });

    $('.slick-slide').css('width', $('.slider-window').width());

    $('.title-banner_slider').css('height', $('.slider-window').width() * 0.4968);

    //height and width of "my works gallery" items
    var colW = $('.my-works__gallery').width();
    $('.my-works__gallery').find('.col').css('width', colW/4);
    $('.my-works__gallery').find('.col').css('height', colW/4);

    var allGalleryLines = $('.gallery-wrap').find('.my-works__gallery');
    for(var i = 1; i < allGalleryLines.length; i++)
    {
        $(allGalleryLines[i]).css('display', 'none');
    }

    //pressing the button
    $('.load-more-gallery__btn').click(function(){
        if($(this).attr('alt') == 1) {
            $(this).text('Hide');
            for (var i = 1; i < allGalleryLines.length; i++) {
                $(allGalleryLines[i]).css({
                    'display': 'block',
                    'width': '100%'
                });
                $('.col').each(function () {
                    $(this).css({"display": "inline-block"});
                    $(this).find('a').find('img').css({
                        'display': 'block',
                        'margin': '0',
                        'padding': '0',
                        'width': '100%',
                        'height': '100%'
                    });
                });
            }
            $(this).attr('alt', 0);
        }else
        {
            $(this).text('Load more');
            for (var i = 1; i < allGalleryLines.length; i++) {
                $(allGalleryLines[i]).css('display', 'none');
            }
            $(this).attr('alt', 1);
        }

    });

});

