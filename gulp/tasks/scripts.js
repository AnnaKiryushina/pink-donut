module.exports = function (){ //Обработка файлов из библиотек и конкотенация их в один файл
    $.gulp.task('scripts:lib', function () {
        return $.gulp.src(['node_modules/jquery/dist/jquery.min.js',
            'node_modules/slick-carousel/slick/slick.min.js'])
            .pipe($.gp.concat('libs.min.js'))
            .pipe($.gulp.dest('build/static/js/'))
            .pipe($.bs.reload({
                stream:true
            }));
    });

    //Копирование main.js в папку build
    $.gulp.task('scripts', function () {
        return $.gulp.src('src/static/js/main.js')
            .pipe($.gulp.dest('build/static/js/'))
            .pipe($.bs.reload({
                stream:true
            }));
    });
}